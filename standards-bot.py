import discord
import json

with open('standards/39a.json', 'r') as f:
    _39a = json.load(f)
with open('standards/95t.json', 'r') as f:
    _95t = json.load(f)
with open('standards/ac.json', 'r') as f:
    ac = json.load(f)
with open('standards/bcf.json', 'r') as f:
    bcf = json.load(f)
with open('standards/ccc.json', 'r') as f:
    ccc = json.load(f)
with open('standards/cda.json', 'r') as f:
    cda = json.load(f)
with open('standards/cdr.json', 'r') as f:
    cdr = json.load(f)
with open('standards/csbi.json', 'r') as f:
    csbi = json.load(f)
with open('standards/hc.json', 'r') as f:
    hc = json.load(f)
with open('standards/lbcf46.json', 'r') as f:
    lbcf46 = json.load(f)
with open('standards/lbcf89.json', 'r') as f:
    lbcf89 = json.load(f)
with open('standards/scots.json', 'r') as f:
    scots = json.load(f)
with open('standards/shc.json', 'r') as f:
    shc = json.load(f)
with open('standards/spc.json', 'r') as f:
    spc = json.load(f)
with open('standards/wcf.json', 'r') as f:
    wcf = json.load(f)
with open('standards/wlc.json', 'r') as f:
    wlc = json.load(f)
with open('standards/wsc.json', 'r') as f:
    wsc = json.load(f)
with open('token.txt', 'r') as f:
    token = f.read()

codes = {
    '+39A': _39a,
    '+95T': _95t,
    '+AC': ac,
    '+BCF': bcf,
    '+CCC': ccc,
    '+CDA': cda,
    '+CDR': cdr,
    '+CSBI': csbi,
    '+HC': hc,
    '+LBCF46': lbcf46,
    '+LBCF89': lbcf89,
    '+SC': scots,
    '+SHC': shc,
    '+SPC': spc,
    '+WCF': wcf,
    '+WLC': wlc,
    '+WSC': wsc,
}

help_text = f"Use the following codes to access a particular confession, followed by the desired section:\n\n"
help_text += '\n'.join([f"{standard}: {codes[standard]['title']}" for standard in codes])

client = discord.Client()

def standards_parse(code):

    for standard in codes:
        if code.startswith(standard):
            ref = code[len(standard):].lstrip().split('.')
            break
    if 'max' in codes[standard] and int(ref[0]) > codes[standard]['max']:
        return 'Not that many chapters/sections!'
    if 'chptr_max' not in codes[standard]:
        return ''.join(list(codes[standard]['text'][ref[0]]))
    elif len(ref) > 1 and int(ref[1]) > codes[standard]['chptr_max'][ref[0]]:
        return 'Not that many sections in that chapter!'
    if len(ref) == 1:
        return ''.join(list(codes[standard]['text'][ref[0]].values()))
    return codes[standard]['text'][ref[0]][ref[1]]

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('+standards'):
        await message.channel.send(help_text)

    if any(message.content.startswith(i) for i in codes):
        await message.channel.send(standards_parse(message.content))

client.run(token)