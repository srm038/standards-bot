{
    "max": 95,
    "title": "95 Theses",
    "text": {
        "1": [
            "> **Thesis 1**\n",
            "> When our Lord and Master Jesus Christ said, \"Repent\" (Mt 4:17), he willed the entire life of believers to be one of repentance.\n"
        ],
        "2": [
            "> **Thesis 2**\n",
            "> This word cannot be understood as referring to the sacrament of penance, that is, confession and satisfaction, as administered by the clergy.\n"
        ],
        "3": [
            "> **Thesis 3**\n",
            "> Yet it does not mean solely inner repentance; such inner repentance is worthless unless it produces various outward mortification of the flesh.\n"
        ],
        "4": [
            "> **Thesis 4**\n",
            "> The penalty of sin remains as long as the hatred of self (that is, true inner repentance), namely till our entrance into the kingdom of heaven.\n"
        ],
        "5": [
            "> **Thesis 5**\n",
            "> The pope neither desires nor is able to remit any penalties except those imposed by his own authority or that of the canons.\n"
        ],
        "6": [
            "> **Thesis 6**\n",
            "> The pope cannot remit any guilt, except by declaring and showing that it has been remitted by God; or, to be sure, by remitting guilt in cases reserved to his judgment. If his right to grant remission in these cases were disregarded, the guilt would certainly remain unforgiven.\n"
        ],
        "7": [
            "> **Thesis 7**\n",
            "> God remits guilt to no one unless at the same time he humbles him in all things and makes him submissive to the vicar, the priest.\n"
        ],
        "8": [
            "> **Thesis 8**\n",
            "> The penitential canons are imposed only on the living, and, according to the canons themselves, nothing should be imposed on the dying.\n"
        ],
        "9": [
            "> **Thesis 9**\n",
            "> Therefore the Holy Spirit through the pope is kind to us insofar as the pope in his decrees always makes exception of the article of death and of necessity.\n"
        ],
        "10": [
            "> **Thesis 10**\n",
            "> Those priests act ignorantly and wickedly who, in the case of the dying, reserve canonical penalties for purgatory.\n"
        ],
        "11": [
            "> **Thesis 11**\n",
            "> Those tares of changing the canonical penalty to the penalty of purgatory were evidently sown while the bishops slept (Mt 13:25).\n"
        ],
        "12": [
            "> **Thesis 12**\n",
            "> In former times canonical penalties were imposed, not after, but before absolution, as tests of true contrition.\n"
        ],
        "13": [
            "> **Thesis 13**\n",
            "> The dying are freed by death from all penalties, are already dead as far as the canon laws are concerned, and have a right to be released from them.\n"
        ],
        "14": [
            "> **Thesis 14**\n",
            "> Imperfect piety or love on the part of the dying person necessarily brings with it great fear; and the smaller the love, the greater the fear.\n"
        ],
        "15": [
            "> **Thesis 15**\n",
            "> This fear or horror is sufficient in itself, to say nothing of other things, to constitute the penalty of purgatory, since it is very near to the horror of despair.\n"
        ],
        "16": [
            "> **Thesis 16**\n",
            "> Hell, purgatory, and heaven seem to differ the same as despair, fear, and assurance of salvation.\n"
        ],
        "17": [
            "> **Thesis 17**\n",
            "> It seems as though for the souls in purgatory fear should necessarily decrease and love increase.\n"
        ],
        "18": [
            "> **Thesis 18**\n",
            "> Furthermore, it does not seem proved, either by reason or by Scripture, that souls in purgatory are outside the state of merit, that is, unable to grow in love.\n"
        ],
        "19": [
            "> **Thesis 19**\n",
            "> Nor does it seem proved that souls in purgatory, at least not all of them, are certain and assured of their own salvation, even if we ourselves may be entirely certain of it.\n"
        ],
        "20": [
            "> **Thesis 20**\n",
            "> Therefore the pope, when he uses the words \"plenary remission of all penalties,\" does not actually mean \"all penalties,\" but only those imposed by himself.\n"
        ],
        "21": [
            "> **Thesis 21**\n",
            "> Thus those indulgence preachers are in error who say that a man is absolved from every penalty and saved by papal indulgences.\n"
        ],
        "22": [
            "> **Thesis 22**\n",
            "> As a matter of fact, the pope remits to souls in purgatory no penalty which, according to canon law, they should have paid in this life.\n"
        ],
        "23": [
            "> **Thesis 23**\n",
            "> If remission of all penalties whatsoever could be granted to anyone at all, certainly it would be granted only to the most perfect, that is, to very few.\n"
        ],
        "24": [
            "> **Thesis 24**\n",
            "> For this reason most people are necessarily deceived by that indiscriminate and high-sounding promise of release from penalty.\n"
        ],
        "25": [
            "> **Thesis 25**\n",
            "> That power which the pope has in general over purgatory corresponds to the power which any bishop or curate has in a particular way in his own diocese and parish.\n"
        ],
        "26": [
            "> **Thesis 26**\n",
            "> The pope does very well when he grants remission to souls in purgatory, not by the power of the keys, which he does not have, but by way of intercession for them.\n"
        ],
        "27": [
            "> **Thesis 27**\n",
            "> They preach only human doctrines who say that as soon as the money clinks into the money chest, the soul flies out of purgatory.\n"
        ],
        "28": [
            "> **Thesis 28**\n",
            "> It is certain that when money clinks in the money chest, greed and avarice can be increased; but when the church intercedes, the result is in the hands of God alone.\n"
        ],
        "29": [
            "> **Thesis 29**\n",
            "> Who knows whether all souls in purgatory wish to be redeemed, since we have exceptions in St. Severinus and St. Paschal, as related in a legend.\n"
        ],
        "30": [
            "> **Thesis 30**\n",
            "> No one is sure of the integrity of his own contrition, much less of having received plenary remission.\n"
        ],
        "31": [
            "> **Thesis 31**\n",
            "> The man who actually buys indulgences is as rare as he who is really penitent; indeed, he is exceedingly rare.\n"
        ],
        "32": [
            "> **Thesis 32**\n",
            "> Those who believe that they can be certain of their salvation because they have indulgence letters will be eternally damned, together with their teachers.\n"
        ],
        "33": [
            "> **Thesis 33**\n",
            "> Men must especially be on guard against those who say that the pope's pardons are that inestimable gift of God by which man is reconciled to him.\n"
        ],
        "34": [
            "> **Thesis 34**\n",
            "> For the graces of indulgences are concerned only with the penalties of sacramental satisfaction established by man.\n"
        ],
        "35": [
            "> **Thesis 35**\n",
            "> They who teach that contrition is not necessary on the part of those who intend to buy souls out of purgatory or to buy confessional privileges preach unchristian doctrine.\n"
        ],
        "36": [
            "> **Thesis 36**\n",
            "> Any truly repentant Christian has a right to full remission of penalty and guilt, even without indulgence letters.\n"
        ],
        "37": [
            "> **Thesis 37**\n",
            "> Any true Christian, whether living or dead, participates in all the blessings of Christ and the church; and this is granted him by God, even without indulgence letters.\n"
        ],
        "38": [
            "> **Thesis 38**\n",
            "> Nevertheless, papal remission and blessing are by no means to be disregarded, for they are, as I have said (Thesis 6), the proclamation of the divine remission.\n"
        ],
        "39": [
            "> **Thesis 39**\n",
            "> It is very difficult, even for the most learned theologians, at one and the same time to commend to the people the bounty of indulgences and the need of true contrition.\n"
        ],
        "40": [
            "> **Thesis 40**\n",
            "> A Christian who is truly contrite seeks and loves to pay penalties for his sins; the bounty of indulgences, however, relaxes penalties and causes men to hate them -- at least it furnishes occasion for hating them.\n"
        ],
        "41": [
            "> **Thesis 41**\n",
            "> Papal indulgences must be preached with caution, lest people erroneously think that they are preferable to other good works of love.\n"
        ],
        "42": [
            "> **Thesis 42**\n",
            "> Christians are to be taught that the pope does not intend that the buying of indulgences should in any way be compared with works of mercy.\n"
        ],
        "43": [
            "> **Thesis 43**\n",
            "> Christians are to be taught that he who gives to the poor or lends to the needy does a better deed than he who buys indulgences.\n"
        ],
        "44": [
            "> **Thesis 44**\n",
            "> Because love grows by works of love, man thereby becomes better. Man does not, however, become better by means of indulgences but is merely freed from penalties.\n"
        ],
        "45": [
            "> **Thesis 45**\n",
            "> Christians are to be taught that he who sees a needy man and passes him by, yet gives his money for indulgences, does not buy papal indulgences but God's wrath.\n"
        ],
        "46": [
            "> **Thesis 46**\n",
            "> Christians are to be taught that, unless they have more than they need, they must reserve enough for their family needs and by no means squander it on indulgences.\n"
        ],
        "47": [
            "> **Thesis 47**\n",
            "> Christians are to be taught that they buying of indulgences is a matter of free choice, not commanded.\n"
        ],
        "48": [
            "> **Thesis 48**\n",
            "> Christians are to be taught that the pope, in granting indulgences, needs and thus desires their devout prayer more than their money.\n"
        ],
        "49": [
            "> **Thesis 49**\n",
            "> Christians are to be taught that papal indulgences are useful only if they do not put their trust in them, but very harmful if they lose their fear of God because of them.\n"
        ],
        "50": [
            "> **Thesis 50**\n",
            "> Christians are to be taught that if the pope knew the exactions of the indulgence preachers, he would rather that the basilica of St. Peter were burned to ashes than built up with the skin, flesh, and bones of his sheep.\n"
        ],
        "51": [
            "> **Thesis 51**\n",
            "> Christians are to be taught that the pope would and should wish to give of his own money, even though he had to sell the basilica of St. Peter, to many of those from whom certain hawkers of indulgences cajole money.\n"
        ],
        "52": [
            "> **Thesis 52**\n",
            "> It is vain to trust in salvation by indulgence letters, even though the indulgence commissary, or even the pope, were to offer his soul as security.\n"
        ],
        "53": [
            "> **Thesis 53**\n",
            "> They are the enemies of Christ and the pope who forbid altogether the preaching of the Word of God in some churches in order that indulgences may be preached in others.\n"
        ],
        "54": [
            "> **Thesis 54**\n",
            "> Injury is done to the Word of God when, in the same sermon, an equal or larger amount of time is devoted to indulgences than to the Word.\n"
        ],
        "55": [
            "> **Thesis 55**\n",
            "> It is certainly the pope's sentiment that if indulgences, which are a very insignificant thing, are celebrated with one bell, one procession, and one ceremony, then the gospel, which is the very greatest thing, should be preached with a hundred bells, a hundred processions, a hundred ceremonies.\n"
        ],
        "56": [
            "> **Thesis 56**\n",
            "> The true treasures of the church, out of which the pope distributes indulgences, are not sufficiently discussed or known among the people of Christ.\n"
        ],
        "57": [
            "> **Thesis 57**\n",
            "> That indulgences are not temporal treasures is certainly clear, for many indulgence sellers do not distribute them freely but only gather them.\n"
        ],
        "58": [
            "> **Thesis 58**\n",
            "> Nor are they the merits of Christ and the saints, for, even without the pope, the latter always work grace for the inner man, and the cross, death, and hell for the outer man.\n"
        ],
        "59": [
            "> **Thesis 59**\n",
            "> St. Lawrence said that the poor of the church were the treasures of the church, but he spoke according to the usage of the word in his own time.\n"
        ],
        "60": [
            "> **Thesis 60**\n",
            "> Without want of consideration we say that the keys of the church, given by the merits of Christ, are that treasure.\n"
        ],
        "61": [
            "> **Thesis 61**\n",
            "> For it is clear that the pope's power is of itself sufficient for the remission of penalties and cases reserved by himself.\n"
        ],
        "62": [
            "> **Thesis 62**\n",
            "> The true treasure of the church is the most holy gospel of the glory and grace of God.\n"
        ],
        "63": [
            "> **Thesis 63**\n",
            "> But this treasure is naturally most odious, for it makes the first to be last (Mt. 20:16).\n"
        ],
        "64": [
            "> **Thesis 64**\n",
            "> On the other hand, the treasure of indulgences is naturally most acceptable, for it makes the last to be first.\n"
        ],
        "65": [
            "> **Thesis 65**\n",
            "> Therefore the treasures of the gospel are nets with which one formerly fished for men of wealth.\n"
        ],
        "66": [
            "> **Thesis 66**\n",
            "> The treasures of indulgences are nets with which one now fishes for the wealth of men.\n"
        ],
        "67": [
            "> **Thesis 67**\n",
            "> The indulgences which the demagogues acclaim as the greatest graces are actually understood to be such only insofar as they promote gain.\n"
        ],
        "68": [
            "> **Thesis 68**\n",
            "> They are nevertheless in truth the most insignificant graces when compared with the grace of God and the piety of the cross.\n"
        ],
        "69": [
            "> **Thesis 69**\n",
            "> Bishops and curates are bound to admit the commissaries of papal indulgences with all reverence.\n"
        ],
        "70": [
            "> **Thesis 70**\n",
            "> But they are much more bound to strain their eyes and ears lest these men preach their own dreams instead of what the pope has commissioned.\n"
        ],
        "71": [
            "> **Thesis 71**\n",
            "> Let him who speaks against the truth concerning papal indulgences be anathema and accursed.\n"
        ],
        "72": [
            "> **Thesis 72**\n",
            "> But let him who guards against the lust and license of the indulgence preachers be blessed.\n"
        ],
        "73": [
            "> **Thesis 73**\n",
            "> Just as the pope justly thunders against those who by any means whatever contrive harm to the sale of indulgences.\n"
        ],
        "74": [
            "> **Thesis 74**\n",
            "> Much more does he intend to thunder against those who use indulgences as a pretext to contrive harm to holy love and truth.\n"
        ],
        "75": [
            "> **Thesis 75**\n",
            "> To consider papal indulgences so great that they could absolve a man even if he had done the impossible and had violated the mother of God is madness.\n"
        ],
        "76": [
            "> **Thesis 76**\n",
            "> We say on the contrary that papal indulgences cannot remove the very least of venial sins as far as guilt is concerned.\n"
        ],
        "77": [
            "> **Thesis 77**\n",
            "> To say that even St. Peter if he were now pope, could not grant greater graces is blasphemy against St. Peter and the pope.\n"
        ],
        "78": [
            "> **Thesis 78**\n",
            "> We say on the contrary that even the present pope, or any pope whatsoever, has greater graces at his disposal, that is, the gospel, spiritual powers, gifts of healing, etc., as it is written. (1 Co 12[:28])\n"
        ],
        "79": [
            "> **Thesis 79**\n",
            "> To say that the cross emblazoned with the papal coat of arms, and set up by the indulgence preachers is equal in worth to the cross of Christ is blasphemy.\n"
        ],
        "80": [
            "> **Thesis 80**\n",
            "> The bishops, curates, and theologians who permit such talk to be spread among the people will have to answer for this.\n"
        ],
        "81": [
            "> **Thesis 81**\n",
            "> This unbridled preaching of indulgences makes it difficult even for learned men to rescue the reverence which is due the pope from slander or from the shrewd questions of the laity.\n"
        ],
        "82": [
            "> **Thesis 82**\n",
            "> Such as: \"Why does not the pope empty purgatory for the sake of holy love and the dire need of the souls that are there if he redeems an infinite number of souls for the sake of miserable money with which to build a church?\" The former reason would be most just; the latter is most trivial.\n"
        ],
        "83": [
            "> **Thesis 83**\n",
            "> Again, \"Why are funeral and anniversary masses for the dead continued and why does he not return or permit the withdrawal of the endowments founded for them, since it is wrong to pray for the redeemed?\"\n"
        ],
        "84": [
            "> **Thesis 84**\n",
            "> Again, \"What is this new piety of God and the pope that for a consideration of money they permit a man who is impious and their enemy to buy out of purgatory the pious soul of a friend of God and do not rather, beca use of the need of that pious and beloved soul, free it for pure love's sake?\"\n"
        ],
        "85": [
            "> **Thesis 85**\n",
            "> Again, \"Why are the penitential canons, long since abrogated and dead in actual fact and through disuse, now satisfied by the granting of indulgences as though they were still alive and in force?\"\n"
        ],
        "86": [
            "> **Thesis 86**\n",
            "> Again, \"Why does not the pope, whose wealth is today greater than the wealth of the richest Crassus, build this one basilica of St. Peter with his own money rather than with the money of poor believers?\"\n"
        ],
        "87": [
            "> **Thesis 87**\n",
            "> Again, \"What does the pope remit or grant to those who by perfect contrition already have a right to full remission and blessings?\"\n"
        ],
        "88": [
            "> **Thesis 88**\n",
            "> Again, \"What greater blessing could come to the church than if the pope were to bestow these remissions and blessings on every believer a hundred times a day, as he now does but once?\"\n"
        ],
        "89": [
            "> **Thesis 89**\n",
            "\"> Since the pope seeks the salvation of souls rather than money by his indulgences, why does he suspend the indulgences and pardons previously granted when they have equal efficacy?\"\n"
        ],
        "90": [
            "> **Thesis 90**\n",
            "> To repress these very sharp arguments of the laity by force alone, and not to resolve them by giving reasons, is to expose the church and the pope to the ridicule of their enemies and to make Christians unhappy.\n"
        ],
        "91": [
            "> **Thesis 91**\n",
            "> If, therefore, indulgences were preached according to the spirit and intention of the pope, all these doubts would be readily resolved. Indeed, they would not exist.\n"
        ],
        "92": [
            "> **Thesis 92**\n",
            "> Away, then, with all those prophets who say to the people of Christ, \"Peace, peace,\" and there is no peace! (Jer 6:14)\n"
        ],
        "93": [
            "> **Thesis 93**\n",
            "> Blessed be all those prophets who say to the people of Christ, \"Cross, cross,\" and there is no cross!\n"
        ],
        "94": [
            "> **Thesis 94**\n",
            "> Christians should be exhorted to be diligent in following Christ, their Head, through penalties, death and hell.\n"
        ],
        "95": [
            "> **Thesis 95**\n",
            "> And thus be confident of entering into heaven through many tribulations rather than through the false security of peace (Acts 14:22).\n"
        ]
    }
}